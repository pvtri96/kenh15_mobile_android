package com.example.tony.kenh15.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tony.kenh15.R;
import com.example.tony.kenh15.activity.SingleArticleActivity;
import com.example.tony.kenh15.model.Article;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.TreeSet;

/**
 * Created by Pham Van Tri on 11/4/2016.
 */

public class ArticlesAdapter extends BaseAdapter {
    private static final int NORMAL_ITEM = 0;
    private static final int HIGHLIGHT_ITEM = 1;
    private static final int TOTAL_TYPE = 2;

    private ArrayList<Article> data = new ArrayList<>();
    private LayoutInflater inflater;
    private Context context;

    //Store highlight item position
    private TreeSet highlightItemSet = new TreeSet();

    private boolean isDisplayCategoryName = true;

    public ArticlesAdapter(Context context) {
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public ArticlesAdapter(Context context, boolean isDisplayCategoryName) {
        this(context);
        this.isDisplayCategoryName = isDisplayCategoryName;
    }

    public boolean isDisplayCategoryName() {
        return isDisplayCategoryName;
    }

    public void setDisplayCategoryName(boolean displayCategoryName) {
        isDisplayCategoryName = displayCategoryName;
    }

    public void addNormalItem(Article article) {
        data.add(article);
        notifyDataSetChanged();
    }

    public void addHighlightItem(Article article) {
        data.add(article);
        highlightItemSet.add(data.size() - 1);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return highlightItemSet.contains(position) ? HIGHLIGHT_ITEM : NORMAL_ITEM;
    }

    @Override
    public int getViewTypeCount() {
        return TOTAL_TYPE;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        int type = getItemViewType(position);
        if (convertView == null) {
            holder = new ViewHolder();
            switch (type) {
                case NORMAL_ITEM:
                    convertView = inflater.inflate(R.layout.list_item_article_extend, null);
                    //Bind view
                    holder.title = (TextView) convertView.findViewById(R.id.article_title);
                    holder.category = (TextView) convertView.findViewById(R.id.category_name);
                    holder.createdAt = (TextView) convertView.findViewById(R.id.article_created);
                    holder.image = (ImageView) convertView.findViewById(R.id.article_img);
                    break;
                case HIGHLIGHT_ITEM:
                    convertView = inflater.inflate(R.layout.list_item_article_large, null);
                    //Bind view
                    holder.title = (TextView) convertView.findViewById(R.id.article_title);
                    holder.description = (TextView) convertView.findViewById(R.id.article_description);
                    holder.image = (ImageView) convertView.findViewById(R.id.article_img);
                    break;
            }

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        //Bind data
        switch (type) {
            case NORMAL_ITEM:
                holder.title.setText(((Article) getItem(position)).getTitle());
                holder.category.setText(isDisplayCategoryName ? ((Article) getItem(position)).getCategoryName() : "");
                holder.createdAt.setText(((Article) getItem(position)).getLastCreated(context.getResources()));
                Picasso.with(context).load(((Article) getItem(position)).getImgUrl()).fit().centerInside().into(holder.image);
                break;
            case HIGHLIGHT_ITEM:
                holder.title.setText(((Article) getItem(position)).getTitle());
                holder.description.setText(((Article) getItem(position)).getDescription());
                Picasso.with(context).load(((Article) getItem(position)).getImgUrl()).fit().centerInside().into(holder.image);
                break;
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SingleArticleActivity.class);
                intent.putExtra("article", ((Article) getItem(position)));
                context.startActivity(intent);
            }
        });

        return convertView;
    }

    private static class ViewHolder {
        TextView title;
        TextView createdAt;
        TextView category;
        TextView description;
        ImageView image;
    }
}
