package com.example.tony.kenh15.fragment;

import android.app.ProgressDialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.tony.kenh15.R;
import com.example.tony.kenh15.adapter.ArticlesAdapter;
import com.example.tony.kenh15.adapter.CategoryAdapter;
import com.example.tony.kenh15.model.Article;
import com.example.tony.kenh15.model.Category;
import com.example.tony.kenh15.service.WebService;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class LatestArticlesFragment extends Fragment {

    private final int ARTICLES_QUANTITY = 5;

    private ProgressDialog progressDialog;
    private ArticlesAdapter articleAdapter;
    private CategoryAdapter categoryAdapter;
    private LinearLayout listView;
    private ArrayList<Article> listArticles;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_latest_articles, container, false);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage(getResources().getString(R.string.please_wait));


        articleAdapter = new ArticlesAdapter(getContext());
        categoryAdapter = new CategoryAdapter(getContext());

        listView = (LinearLayout) (view.findViewById(R.id.latest_articles));

        httpGetArticle();

        // Inflate the layout for this fragment
        return view;
    }

    private void httpGetArticle() {
        progressDialog.show();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(WebService.selectArticles(), null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String response) {
                progressDialog.hide();
                try {
                    JSONArray jsonArray = new JSONArray(response);

                    listArticles = Article.fromJSON(jsonArray);

                    articleAdapter.addHighlightItem(listArticles.remove(listArticles.size() - 1));

                    Collections.reverse(listArticles);
                    for (int i = 0; i < listArticles.size(); i++) {
                        articleAdapter.addNormalItem(listArticles.get(i));
                    }

                    for (int i = 0; i < articleAdapter.getCount() && i < ARTICLES_QUANTITY; i++) {
                        View view = articleAdapter.getView(i, null, listView);
                        listView.addView(view);
                    }

                    getCategories();

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                Toast.makeText(getContext(), getResources().getString(R.string.loading_success), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int statusCode, Throwable error, String content) {
                // Hide Progress Dialog
                progressDialog.hide();
                // When Http response code is '500'
                if (statusCode == 500) {
                    Toast.makeText(getContext(), getResources().getString(R.string.loading_fail_status_500), Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(getContext(), getResources().getString(R.string.loading_fail), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void getCategories() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(WebService.selectCategories(), null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    ArrayList<Category> listCategories = Category.fromJSON(jsonArray);

                    // filter hot category
                    for (int i = 0; i < listCategories.size(); ) {
                        if (!listCategories.get(i).is_hot()) {
                            listCategories.remove(i);
                        } else i++;
                    }

                    // sort category by article quantity
                    Collections.sort(listCategories, new Comparator<Category>() {
                        @Override
                        public int compare(Category o1, Category o2) {
                            if (o1.getArticles() > o2.getArticles()) return -1;
                            if (o1.getArticles() < o2.getArticles()) return 1;
                            return 0;
                        }
                    });

                    categoryAdapter.setData(listCategories);
                    for (int i = 0; i < categoryAdapter.getCount(); i++) {
                        View view = categoryAdapter.getView(i, null, listView);
                        listView.addView(view);
                    }

                    for (int i = ARTICLES_QUANTITY; i < articleAdapter.getCount(); i++) {
                        View view = articleAdapter.getView(i, null, listView);
                        listView.addView(view);
                        if (i == ARTICLES_QUANTITY) {
                            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view.getLayoutParams();

                            // Convert dp to px
                            Resources r = getResources();
                            int marginTop = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, r.getDimension(R.dimen.latest_article_list_margin_top), r.getDisplayMetrics());
                            params.setMargins(0, marginTop, 0, 0);

                            view.setLayoutParams(params);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Throwable error, String content) {
                // Hide Progress Dialog
                // When Http response code is '500'
                if (statusCode == 500) {
                    Toast.makeText(getContext(), getResources().getString(R.string.loading_fail_status_500), Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(getContext(), getResources().getString(R.string.loading_fail), Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
