package com.example.tony.kenh15.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.tony.kenh15.R;
import com.example.tony.kenh15.activity.CategoryActivity;
import com.example.tony.kenh15.model.Article;
import com.example.tony.kenh15.model.Category;
import com.example.tony.kenh15.service.WebService;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Le Thi Thuy Dung on 11/6/2016.
 */

public class CategoryAdapter extends BaseAdapter {

    private final int ARTICLES_QUANTITY=2;

    private LayoutInflater inflater;
    private Context context;
    private ArrayList<Category> data = new ArrayList<>();
    private ArticlesAdapter articlesAdapter;

    public CategoryAdapter(Context applicationAdapter) {
        this.context = applicationAdapter;
        inflater = (LayoutInflater.from(applicationAdapter));
    }

    public ArrayList<Category> getData() {
        return data;
    }

    public void setData(ArrayList<Category> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    private static class ViewHolder {
        TextView name;
        LinearLayout listArticles;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.list_item_category_extend, null);
            viewHolder.name = (TextView) convertView.findViewById(R.id.category_name);
            viewHolder.listArticles = (LinearLayout) convertView.findViewById(R.id.list_articles_by_category);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.name.setText(((Category) getItem(position)).getName());
        convertView.findViewById(R.id.category_info).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CategoryActivity.class);
                intent.putExtra("category", (Category) getItem(position));
                context.startActivity(intent);
            }
        });

        getArticles((Category) getItem(position), viewHolder);

        return convertView;
    }

    public void getArticles(final Category category, final ViewHolder viewHolder) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(WebService.selectCategory(category.getUrl()), null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String response) {
                articlesAdapter = new ArticlesAdapter(context);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    ArrayList<Article> list = Article.fromJSON(jsonArray);
                    for (int i = 0; i < list.size() && i < ARTICLES_QUANTITY; i++) {
                        articlesAdapter.addNormalItem(list.get(i));
                        View view = articlesAdapter.getView(i, null, viewHolder.listArticles);
                        viewHolder.listArticles.addView(view);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Throwable error, String content) {
                getArticles(category, viewHolder);
            }
        });
    }

}
