package com.example.tony.kenh15.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.example.tony.kenh15.R;
import com.example.tony.kenh15.adapter.ArticlesAdapter;
import com.example.tony.kenh15.model.Article;
import com.example.tony.kenh15.service.WebService;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class TopArticlesFragment extends Fragment {
    private ArticlesAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_top_articles, container, false);

        adapter = new ArticlesAdapter(getContext());

        ListView listView = (ListView) (view.findViewById(R.id.list_view_articles));
        listView.setAdapter(adapter);

        httpGetArticle();
        return view;
    }

    private void httpGetArticle() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(WebService.selectArticles(), null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);

                    ArrayList<Article> list = Article.fromJSON(jsonArray);

                    Collections.sort(list, new Comparator<Article>() {
                        @Override
                        public int compare(Article article1, Article article2) {
                            if (article1.getViews() < article2.getViews()) return 1;
                            if (article1.getViews() > article2.getViews()) return -1;
                            return 0;
                        }
                    });

                    for (int i = 0; i < list.size(); i++) {
                        if (i == 0) adapter.addHighlightItem(list.get(i));
                        else adapter.addNormalItem(list.get(i));
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                Toast.makeText(getContext(), getResources().getString(R.string.loading_success), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int statusCode, Throwable error, String content) {
                httpGetArticle();
            }
        });
    }
}
