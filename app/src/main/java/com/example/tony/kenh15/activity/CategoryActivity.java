package com.example.tony.kenh15.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.example.tony.kenh15.R;
import com.example.tony.kenh15.adapter.ArticlesAdapter;
import com.example.tony.kenh15.model.Article;
import com.example.tony.kenh15.model.Category;
import com.example.tony.kenh15.service.WebService;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;

import java.util.ArrayList;

public class CategoryActivity extends AppCompatActivity {

    ArticlesAdapter adapter;

    Category category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        category = (Category) getIntent().getSerializableExtra("category");
        Log.d("category", category.getName() + "");
        setTitle(category.getName());


        //Set back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        adapter = new ArticlesAdapter(this, false);

        ListView listView = (ListView) (findViewById(R.id.list_view_articles));
        listView.setAdapter(adapter);

        httpGetArticle();
    }

    private void httpGetArticle() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(WebService.selectCategory(category.getUrl()), null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);

                    ArrayList<Article> list = Article.fromJSON(jsonArray);

                    adapter.addHighlightItem(list.remove(0));
                    for (int i = 0; i < list.size(); i++) {
                        adapter.addNormalItem(list.get(i));
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.loading_success), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int statusCode, Throwable error, String content) {
                httpGetArticle();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
