package com.example.tony.kenh15.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tony.kenh15.R;
import com.example.tony.kenh15.adapter.ArticlesAdapter;
import com.example.tony.kenh15.model.Article;
import com.example.tony.kenh15.model.Category;
import com.example.tony.kenh15.model.Tag;
import com.example.tony.kenh15.service.WebService;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.sufficientlysecure.htmltextview.HtmlHttpImageGetter;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.ArrayList;

public class SingleArticleActivity extends AppCompatActivity {

    private final int ARTICLES_QUANTITY = 5;

    private LinearLayout listRelatedArticles;
    private ArticlesAdapter adapter;
    private Article article;
    private TextView tvTitle, tvCategoryName, tvCreatedAt, tvTags, tvAuthors;
    private HtmlTextView tvContent;
    private ImageView articleIMG;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_article);

        //Set back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        article = (Article) getIntent().getSerializableExtra("article");

        tvTitle = (TextView) findViewById(R.id.article_title);
        tvCategoryName = (TextView) findViewById(R.id.category_name);
        tvCreatedAt = (TextView) findViewById(R.id.article_created);
        tvContent = (HtmlTextView) findViewById(R.id.article_content);
        articleIMG = (ImageView) findViewById(R.id.article_img);
        tvAuthors = (TextView) findViewById(R.id.article_authors);
        tvTags = (TextView) findViewById(R.id.article_tags);
        listRelatedArticles = (LinearLayout) findViewById(R.id.list_related_articles);

        tvCategoryName.setText(article.getCategoryName());
        tvCreatedAt.setText(article.getLastCreated(getResources()));

        Picasso.with(this).load(article.getImgUrl()).fit().centerInside().into(articleIMG);

        adapter = new ArticlesAdapter(this, false);

        tvTitle.setText(article.getTitle());
        setTitle(article.getTitle());

        tvCategoryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CategoryActivity.class);
                Category category = new Category();
                // Unknown bug!!!
                // MUST USE tvCategoryName.getText() instead of article.getCategoryName()
                category.setName(tvCategoryName.getText() + "");
                category.setUrl(article.getCategoryUrl());
                intent.putExtra("category", category);
                startActivity(intent);
            }
        });

        httpGetArticle();
    }

    private void httpGetArticle() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(WebService.selectArticle(article.getUrl()), null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String response) {
                try {
                    article = new Article();
                    JSONObject object = new JSONObject(response);
                    article.setID(object.getInt("id"));
                    article.setTitle(object.getString("title"));
                    article.setContent(object.getString("content"));
                    article.setCategoryUrl(object.getJSONObject("category").getString("url"));

                    /* declare ArrayList<Tag> and ArrayList<String> (Authors)*/
                    ArrayList<Tag> listTags = new ArrayList<Tag>();
                    ArrayList<String> listAuthors = new ArrayList<String>();

                    /* get array Tags and Authors from Article Object*/
                    JSONArray array_tags = object.getJSONArray("tags");
                    JSONArray array_authors = object.getJSONArray("authors");

                    /* add Tag object to ArrayList*/
                    for (int i = 0; i < array_tags.length(); i++) {
                        Tag tag = new Tag(array_tags.getJSONObject(i));
                        listTags.add(tag);
                    }
                    article.setListTags(listTags);

                    /* add Author Name to ArrayList*/
                    for (int i = 0; i < array_authors.length(); i++) {
                        String author = new String(array_authors.getJSONObject(i).getString("name"));
                        listAuthors.add(author);
                    }
                    article.setListAuthors(listAuthors);

                    /*display Tags*/
                    String tags = "";
                    for (int i = 0; i < article.getListTags().size(); i++) {
                        if (i < article.getListTags().size() - 1)
                            tags = tags + article.getListTags().get(i).getName() + ", ";
                        else tags = tags + article.getListTags().get(i).getName();
                    }
                    tvTags.setText(tags);

                    /*display Authors*/
                    String authors = "";
                    for (int i = 0; i < article.getListAuthors().size(); i++) {
                        if (i < article.getListAuthors().size() - 1)
                            authors = authors + article.getListAuthors().get(i) + ", ";
                        else authors = authors + article.getListAuthors().get(i);
                    }
                    tvAuthors.setText(authors);

                    tvTitle.setText(article.getTitle());
                    tvContent.setHtml(article.getContent(), new HtmlHttpImageGetter(tvContent, null, true));

                    /*get Related Articles*/
                    getRelatedArticles(article);

                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.loading_success), Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Throwable error, String content) {
                httpGetArticle();
            }
        });
    }

    public void getRelatedArticles(final Article article) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(WebService.selectCategory(article.getCategoryUrl()), null, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            ArrayList<Article> list = new ArrayList<Article>();
                            JSONArray categoryJSON = new JSONArray(response);
                            for (int i = 0; i < categoryJSON.length(); i++) {
                                JSONObject articleJSON = categoryJSON.getJSONObject(i);
                                if (article.getID() != articleJSON.getInt("id")) {
                                    Article article = new Article(articleJSON);
                                    list.add(article);
                                }
                            }
                            for (int i = 0; i < list.size() && i < ARTICLES_QUANTITY; i++) {
                                adapter.addNormalItem(list.get(i));
                                View view = adapter.getView(i, null, listRelatedArticles);
                                listRelatedArticles.addView(view);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Throwable error, String content) {
                        getRelatedArticles(article);
                    }
                }
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
