package com.example.tony.kenh15.model;

import android.content.res.Resources;

import com.example.tony.kenh15.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Pham Van Tri and Le Thi Thuy Dung on 11/4/2016.
 */

public class Article implements Serializable {

    private int ID, views;

    private String title, content, url, img_url, description, categoryName, categoryUrl;
    private String created_at, updated_at;
    private ArrayList<Tag> listTags = new ArrayList<Tag>();
    private ArrayList<String> listAuthors = new ArrayList<>();

    public String getCategoryUrl() {
        return categoryUrl;
    }

    public void setCategoryUrl(String categoryUrl) {
        this.categoryUrl = categoryUrl;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }


    public void setViews(int views) {
        this.views = views;
    }

    public int getViews() {
        return views;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImgUrl() {
        return img_url;
    }

    public void setImgUrl(String img_url) {
        this.img_url = img_url;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Article() {
        title = content = url = img_url = "";
    }

    public void setListTags(ArrayList<Tag> listTags) {
        this.listTags = listTags;
    }

    public ArrayList<Tag> getListTags() {
        return listTags;
    }

    public void setListAuthors(ArrayList<String> listAuthors) {
        this.listAuthors = listAuthors;
    }

    public ArrayList<String> getListAuthors() {
        return listAuthors;
    }

    public Article(JSONObject jsonObject) {
        try {
            this.ID = jsonObject.getInt("id");
            this.title = jsonObject.getString("shorten_title");
            this.description = jsonObject.getString("shorten_content");
            this.url = jsonObject.getString("url");
            this.img_url = jsonObject.getString("img_url");
            this.views = jsonObject.getInt("views");
            this.categoryName = jsonObject.getString("category_name");
            this.created_at = jsonObject.getJSONObject("created_at").getString("date");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public Article(String title) {
        this.title = title;
        content = url = img_url = "";
    }

    @Override
    public String toString() {
        return this.title;
    }

    public static ArrayList<Article> fromJSON(JSONArray jsonArray) {
        ArrayList<Article> list = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                list.add(new Article(jsonArray.getJSONObject(i)));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return list;
    }

    /**
     * Processing Date of Article
     * Created by Le Thi Thuy Dung
     */
    public String getLastCreated(Resources resources) {
        Date startDate = convertDate(getCreated_at());
        Date date = new Date();
        return differenceDate(startDate, date, resources);
    }

    public Date convertDate(String dateString) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date convertDate = new Date();
        try {
            convertDate = sdf.parse(dateString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertDate;
    }

    public String differenceDate(Date startDate, Date endDate, Resources resources) {
        // million seconds
        long different = endDate.getTime() - startDate.getTime();

        final long secondsInMilli = 1000;
        final long minutesInMilli = secondsInMilli * 60;
        final long hoursInMilli = minutesInMilli * 60;
        final long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedMonths = new Long(elapsedDays / 30);

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different * minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        if ((elapsedDays == 0 && elapsedHours == 0) && elapsedSeconds < 60) {
            return resources.getString(R.string.just_now);
        }
        if ((elapsedDays == 0 && elapsedHours == 0) && elapsedMinutes >= 1) {
            return elapsedMinutes + " " + resources.getString(R.string.minutes_ago);
        }
        if (elapsedDays == 0 && elapsedHours >= 1) {
            return elapsedHours + " " + resources.getString(R.string.hours_ago);
        }
        if (elapsedDays < 30 && elapsedDays >= 1) {
            return elapsedDays + " " + resources.getString(R.string.days_ago);
        }
        if (elapsedDays >= 30) {
            return elapsedMonths + " " + resources.getString(R.string.months_ago);
        }
        return "";
    }
}
