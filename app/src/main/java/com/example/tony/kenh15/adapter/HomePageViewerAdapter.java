package com.example.tony.kenh15.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.example.tony.kenh15.fragment.*;

/**
 * Created by Pham Van Tri
 * Control the tab layout in HomeActivity
 */

public class HomePageViewerAdapter extends FragmentStatePagerAdapter {

    private int numOfTabs;

    public HomePageViewerAdapter(FragmentManager fm, int numOfTabs){
        super(fm);
        this.numOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                LatestArticlesFragment latestArticlesFragment = new LatestArticlesFragment();
                return latestArticlesFragment;
            case 1:
                TopArticlesFragment topArticlesFragment = new TopArticlesFragment();
                return topArticlesFragment;
            case 2:
                MyArticlesFragment myArticlesFragment = new MyArticlesFragment();
                return myArticlesFragment;
            case 3:
                TopVideosFragment topVideosFragment = new TopVideosFragment();
                return topVideosFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
