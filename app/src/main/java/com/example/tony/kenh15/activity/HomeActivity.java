package com.example.tony.kenh15.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.tony.kenh15.R;
import com.example.tony.kenh15.adapter.HomePageViewerAdapter;
import com.example.tony.kenh15.model.Category;
import com.example.tony.kenh15.service.WebService;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*Set drawer navigation bar*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Menu menu = navigationView.getMenu();
        addDrawerMenuItem(menu);

        /*End drawer navigation bar*/

        /*Set TabLayout*/

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.latest_news)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.top_news)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.my_news)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.top_videos)));

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final HomePageViewerAdapter adapter = new HomePageViewerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        /*End TabLayout*/

    }

    private void addDrawerMenuItem(final Menu menu) {

        new AsyncHttpClient().get(WebService.selectCategories(), null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);

                    ArrayList<Category> list = Category.fromJSON(jsonArray);

                    for (int i = 0; i < list.size();i++ ) {
                        if (!list.get(i).is_hot())
                            list.remove(i--);
                    }

                    Collections.sort(list, new Comparator<Category>() {
                        @Override
                        public int compare(Category o1, Category o2) {
                            if (o1.getArticles() > o2.getArticles()) return -1;
                            if (o1.getArticles() < o2.getArticles()) return 1;
                            return 0;
                        }
                    });

                    for (int i = 0; i < list.size(); i++) {
                        MenuItem menuItem = menu.add(list.get(i).getName());

                        //Bind CategoryActivity Intent to each menu Item
                        Intent intent = new Intent(getApplicationContext(), CategoryActivity.class);
                        intent.putExtra("category", list.get(i));
                        menuItem.setIntent(intent);
                        //End Bind CategoryActivity
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable error) {
                addDrawerMenuItem(menu);
            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);

        switch (item.getItemId()) {
            case R.id.nav_latest_articles:
                viewPager.setCurrentItem(0);
                break;
            case R.id.nav_top_articles:
                viewPager.setCurrentItem(1);
                break;
            case R.id.nav_my_articles:
                viewPager.setCurrentItem(2);
                break;
            case R.id.nav_top_videos:
                viewPager.setCurrentItem(3);
                break;
            default:
                //Start Category activity
                startActivity(item.getIntent());
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
