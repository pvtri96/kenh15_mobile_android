package com.example.tony.kenh15.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Le Thi Thuy Dung on 11/5/2016.
 */

public class Tag implements Serializable {
    private int id;
    private String name, url, note;
    private Calendar created_at, updated_at;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setCreated_at(Calendar created_at) {
        this.created_at = created_at;
    }

    public void setUpdated_at(Calendar updated_at) {
        this.updated_at = updated_at;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String getNote() {
        return note;
    }

    public Tag() {
        name = url = note = "";
    }

    public Tag(String name) {
        id = 0;
        this.name = name;
        url = note = "";
        created_at = updated_at = Calendar.getInstance();
    }

    public Tag(JSONObject jsonObject) {
        try {
            this.id = jsonObject.getInt("id");
            this.name = jsonObject.getString("name");
            this.url = jsonObject.getString("url");
            this.note = jsonObject.getString("note");
            // created_at
            // updated_at
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Tag> fromJSON(JSONArray jsonArray) {
        ArrayList<Tag> list = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                list.add(new Tag(jsonArray.getJSONObject(i)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
