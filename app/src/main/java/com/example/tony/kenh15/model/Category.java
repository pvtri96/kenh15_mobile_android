package com.example.tony.kenh15.model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Le Thi Thuy Dung on 11/5/2016.
 */

public class Category implements Serializable {
    private int id;
    private String name, url, note;
    private boolean is_hot, is_header;
    private int articles;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setIs_hot(boolean is_hot) {
        this.is_hot = is_hot;
    }

    public void setIs_header(boolean is_header) {
        this.is_header = is_header;
    }

    public void setArticles(int articles) {
        this.articles = articles;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String getNote() {
        return note;
    }

    public boolean is_hot() {
        return is_hot;
    }

    public boolean is_header() {
        return is_header;
    }

    public int getArticles() {
        return articles;
    }

    public Category() {
        name = url = note = "";
    }

    public Category(String name) {
        id = 0;
        this.name = name;
        url = note = "";
    }

    public Category(JSONObject jsonObject) {
        try {
            this.id = jsonObject.getInt("id");
            this.name = jsonObject.getString("name");
            this.url = jsonObject.getString("url");
            this.name = jsonObject.getString("note");
            this.articles = jsonObject.getInt("articles");
            this.is_hot = jsonObject.getJSONObject("advance").getString("is_hot").compareTo("0") == 0 ? false : true;
            this.is_header = jsonObject.getJSONObject("advance").getString("is_header").compareTo("0") == 0 ? false : true;
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public static ArrayList<Category> fromJSON(JSONArray jsonArray) {
        ArrayList<Category> list = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                list.add(new Category(jsonArray.getJSONObject(i)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
