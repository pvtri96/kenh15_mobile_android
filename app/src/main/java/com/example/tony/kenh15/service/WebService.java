package com.example.tony.kenh15.service;

/**
 * Created by Pham Van Tri on 11/8/2016.
 */

public class WebService {

    private static String sourceAPI = "http://192.168.43.178";

    private WebService() {
    }

    public static void setSourceAPI(String newSource) {
        sourceAPI = newSource;
    }

    public static String getSourceAPI() {
        return sourceAPI;
    }

    public static String selectCategories() {
        return sourceAPI + "/api/category/select";
    }

    public static String selectCategory(String categoryURL) {
        return sourceAPI + "/api/category/get/articles/category_url=" + categoryURL;
    }

    public static String selectArticles() {
        return sourceAPI + "/api/article/get/all";
    }

    public static String selectArticle(String articleURL) {
        return sourceAPI + "/api/article/get/article_url=" + articleURL;
    }
}
